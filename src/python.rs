pub mod character;
pub mod class;

use pyo3::prelude::*;
use pyo3::types::PyDict;

pub fn charman<'p>(python: Python<'p>) -> PyResult<&'p PyModule> {
    let module = PyModule::new(python, "charman")?;
    module.add_class::<class::Class>()?;
    Ok(module)
}

pub fn init_charman<'p>(python: Python<'p>) -> PyResult<()> {
    let module = charman(python)?;
    let sys = python.import("sys")?;
    let modules: &PyDict = sys.get("modules")?.extract()?;
    modules.set_item("charman", module)?;
    Ok(())
}
