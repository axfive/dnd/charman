use serde::de::{Deserializer, SeqAccess, Visitor};
use serde::ser::{SerializeSeq, Serializer};
use serde::{Deserialize, Serialize};
use std::cell::Ref;
use std::cell::RefCell;
use std::convert::Into;
use std::fmt;
use std::ops::Deref;
use std::rc::Rc;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Skill {
    // short name used for proficiency, like `sleight_of_hand`
    id: String,

    // long name used for translation and display, like `Sleight of Hand`
    name: String,
}

impl Skill {
    // Take a name, derive an id
    pub fn new<S: Into<String>>(name: S) -> Skill {
        let name = name.into();
        let id = name.to_lowercase().replace(' ', "_");
        Skill {
            id,
            name: name.into(),
        }
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Ability {
    // short name used for proficiency lookups, like `strength`
    id: String,

    // long name used for display and translation, like `Strength`
    name: String,

    // The score, not the modifier.  The score will be used to calculate the modifier
    score: i8,

    // A list of skills that this ability supports
    skills: Vec<Skill>,
}

impl Ability {
    pub fn new<S: Into<String>>(name: S) -> Ability {
        let name = name.into();
        let id = name.to_lowercase().replace(' ', "_");
        Ability {
            id,
            name,
            score: 10,
            skills: Default::default(),
        }
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn score(&self) -> i8 {
        self.score
    }

    pub fn set_score(&mut self, score: i8) {
        self.score = score;
    }

    pub fn modifier(&self) -> i8 {
        self.score / 2 - 5
    }

    pub fn skills(&self) -> impl DoubleEndedIterator<Item = &Skill> {
        self.skills.iter()
    }

    pub fn skills_mut(&mut self) -> impl DoubleEndedIterator<Item = &mut Skill> {
        self.skills.iter_mut()
    }

    pub fn add_skill<S: Into<String>>(&mut self, name: S) {
        self.skills.push(Skill::new(name));
    }
}

#[derive(Debug, Clone)]
pub struct Abilities(Vec<Rc<RefCell<Ability>>>);

// We can not derive Serialize/Deserialize for Abilities, because they hold smart references and cells,
// which are fairly complex types
impl Serialize for Abilities {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.0.len()))?;
        for e in &self.0 {
            let item: Ref<Ability> = e.borrow();
            seq.serialize_element(item.deref() as &Ability)?;
        }
        seq.end()
    }
}

struct AbilitiesVisitor;

impl<'de> Visitor<'de> for AbilitiesVisitor {
    type Value = Abilities;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a sequence of items")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: SeqAccess<'de>,
    {
        let mut items: Vec<Rc<RefCell<Ability>>> = Vec::new();
        while let Some(value) = seq.next_element()? {
            items.push(Rc::new(RefCell::new(value)))
        }
        Ok(Abilities(items))
    }
}

impl<'de> Deserialize<'de> for Abilities {
    fn deserialize<D>(deserializer: D) -> Result<Abilities, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_seq(AbilitiesVisitor)
    }
}

/// Construct the default D&D set of abilities and skills all at score 10
impl Default for Abilities {
    fn default() -> Self {
        let mut abilities = Abilities::new();
        let mut strength = Ability::new("Strength");
        strength.add_skill("Athletics");
        abilities.push(strength);
        let mut dexterity = Ability::new("Dexterity");
        dexterity.add_skill("Acrobatics");
        dexterity.add_skill("Sleight of Hand");
        dexterity.add_skill("Stealth");
        abilities.push(dexterity);
        let constitution = Ability::new("Constitution");
        abilities.push(constitution);
        let mut intelligence = Ability::new("Intelligence");
        intelligence.add_skill("Arcana");
        intelligence.add_skill("History");
        intelligence.add_skill("Investigation");
        intelligence.add_skill("Nature");
        intelligence.add_skill("Religion");
        abilities.push(intelligence);
        let mut wisdom = Ability::new("Wisdom");
        wisdom.add_skill("Animal Handling");
        wisdom.add_skill("Insight");
        wisdom.add_skill("Medicine");
        wisdom.add_skill("Perception");
        wisdom.add_skill("Survival");
        abilities.push(wisdom);
        let mut charisma = Ability::new("Charisma");
        charisma.add_skill("Deception");
        charisma.add_skill("Intimidation");
        charisma.add_skill("Performance");
        charisma.add_skill("Persuasion");
        abilities.push(charisma);

        abilities
    }
}

impl Abilities {
    pub fn new() -> Abilities {
        Abilities(Vec::new())
    }

    pub fn iter(&self) -> impl DoubleEndedIterator<Item = Ref<Ability>> {
        self.0.iter().map(|i| i.borrow())
    }

    pub fn iter_rc(&self) -> impl DoubleEndedIterator<Item = Rc<RefCell<Ability>>> + '_ {
        self.0.iter().cloned()
    }

    pub fn push(&mut self, ability: Ability) {
        self.0.push(Rc::new(RefCell::new(ability)));
    }
}
