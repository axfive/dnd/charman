import charman

class Fighter(charman.Class):
    name = 'Fighter'
    id = 'fighter'
    hit_die = 10
    proficiencies = {
        'armor': 1,
        'shield': 1,
        'weapon.simple': 1,
        'weapon.martial': 1,
        'saving_throw.constitution': 1,
        'saving_throw.strength': 1,
    }

    def second(self):
        print("fighter second")
        super().second()

    def third(self):
        print("fighter third")
        super().third()
