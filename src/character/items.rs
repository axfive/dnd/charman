use crate::weight::Weight;
use serde::de::{Deserializer, SeqAccess, Visitor};
use serde::ser::{SerializeSeq, Serializer};
use serde::{Deserialize, Serialize};
use std::cell::Ref;
use std::cell::RefCell;
use std::collections::HashSet;
use std::fmt;
use std::ops::Deref;
use std::rc::Rc;

/// The contents of an item.
///
/// Spellbooks may hold spells, containers may hold items.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Contents {
    Nothing,
    Items(Items),
}

impl Default for Contents {
    fn default() -> Self {
        Contents::Nothing
    }
}

impl Weight for Contents {
    fn weight(&self) -> f32 {
        match self {
            Contents::Nothing => 0.0,
            Contents::Items(items) => items.weight(),
        }
    }
}

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct Item {
    /// Weight per unit
    pub weight: f32,

    /// Number of contained units
    pub quantity: u32,

    pub name: String,
    pub subtitle: String,
    pub description: String,

    pub tags: HashSet<String>,

    pub contents: Contents,
}

impl Weight for Item {
    fn weight(&self) -> f32 {
        self.weight + self.contents.weight()
    }
}

#[derive(Default, Debug, Clone)]
pub struct Items(Vec<Rc<RefCell<Item>>>);

impl Weight for Items {
    fn weight(&self) -> f32 {
        self.0.iter().map(|i| i.borrow().weight()).sum()
    }
}

// We can not derive Serialize/Deserialize for Items, because they hold smart references and cells,
// which are fairly complex types
impl Serialize for Items {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.0.len()))?;
        for e in &self.0 {
            let item: Ref<Item> = e.borrow();
            seq.serialize_element(item.deref() as &Item)?;
        }
        seq.end()
    }
}

struct ItemsVisitor;

impl<'de> Visitor<'de> for ItemsVisitor {
    type Value = Items;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a sequence of items")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: SeqAccess<'de>,
    {
        let mut items: Vec<Rc<RefCell<Item>>> = match seq.size_hint() {
            Some(size) => Vec::with_capacity(size),
            None => Vec::new(),
        };
        while let Some(value) = seq.next_element()? {
            items.push(Rc::new(RefCell::new(value)))
        }
        Ok(Items(items))
    }
}

impl<'de> Deserialize<'de> for Items {
    fn deserialize<D>(deserializer: D) -> Result<Items, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_seq(ItemsVisitor)
    }
}
