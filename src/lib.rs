//! This is a manager for D&D characters.  The core class is
//! [Character](character/struct.Character.html).

pub mod character;
pub mod python;
pub mod weight;
