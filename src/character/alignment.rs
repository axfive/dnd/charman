use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
pub enum Alignment {
    LawfulGood,
    NeutralGood,
    ChaoticGood,
    LawfulNeutral,
    Neutral,
    ChaoticNeutral,
    LawfulEvil,
    NeutralEvil,
    ChaoticEvil,
}

impl Alignment {
    /// get a two-letter ID for the given alignment
    pub fn id_str(&self) -> &'static str {
        match self {
            Alignment::LawfulGood => "lg",
            Alignment::NeutralGood => "ng",
            Alignment::ChaoticGood => "cg",
            Alignment::LawfulNeutral => "ln",
            Alignment::Neutral => "nn",
            Alignment::ChaoticNeutral => "cn",
            Alignment::LawfulEvil => "le",
            Alignment::NeutralEvil => "ne",
            Alignment::ChaoticEvil => "ce",
        }
    }

    pub fn from_id_str(id: &str) -> Option<Alignment> {
        match id {
            "lg" => Some(Alignment::LawfulGood),
            "ng" => Some(Alignment::NeutralGood),
            "cg" => Some(Alignment::ChaoticGood),
            "ln" => Some(Alignment::LawfulNeutral),
            "nn" => Some(Alignment::Neutral),
            "cn" => Some(Alignment::ChaoticNeutral),
            "le" => Some(Alignment::LawfulEvil),
            "ne" => Some(Alignment::NeutralEvil),
            "ce" => Some(Alignment::ChaoticEvil),
            _ => None,
        }
    }
}

impl Default for Alignment {
    fn default() -> Self {
        Alignment::Neutral
    }
}

impl fmt::Display for Alignment {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let string = match self {
            Alignment::LawfulGood => "Lawful Good",
            Alignment::NeutralGood => "Neutral Good",
            Alignment::ChaoticGood => "Chaotic Good",
            Alignment::LawfulNeutral => "Lawful Neutral",
            Alignment::Neutral => "Neutral",
            Alignment::ChaoticNeutral => "Chaotic Neutral",
            Alignment::LawfulEvil => "Lawful Evil",
            Alignment::NeutralEvil => "Neutral Evil",
            Alignment::ChaoticEvil => "Chaotic Evil",
        };
        write!(f, "{}", string)
    }
}
