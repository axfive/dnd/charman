# Contributing

* Keep to the style as presented.  Our style mostly is similar to PEP8, but without hanging indent on closing grouping, and a few other small differences.  All contribution must abide by the MIT license.