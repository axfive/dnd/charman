/// Things that weigh something
/// This is usually a cumulative measurement, showing the weight of something as well as its contents.
pub trait Weight {
    fn weight(&self) -> f32;
}
