use pyo3::exceptions::NotImplementedError;
use pyo3::prelude::*;
use pyo3::types::{IntoPyDict, PyDict};
use std::convert::AsRef;

#[pyclass(subclass, dict, module = "charman")]
pub struct Class {}
#[pymethods]
impl Class {
    #[new]
    fn new() -> Self {
        Class {}
    }

    #[classattr]
    const fn name() -> &'static str {
        "class"
    }

    fn register(_self: PyRef<Self>) -> PyResult<()> {
        NotImplementedError::into("Must subclass Class and override register")
    }

    fn second(self_: &PyCell<Self>) -> PyResult<()> {
        println!("Second");
        let py_ref = self_.borrow();
        let py = py_ref.py();
        let locals = PyDict::new(py);
        locals.set_item("self", self_)?;
        py.eval(
            "self.third()",
            None,
            Some([("self", self_)].into_py_dict(py)),
        )?;
        Ok(())
    }

    fn third(_self: PyRef<Self>) -> PyResult<()> {
        println!("Third");
        Ok(())
    }
}
