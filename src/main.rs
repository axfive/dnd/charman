use charman::python::init_charman;
use clap::{App, Arg};
use pyo3::prelude::*;
use pyo3::types::{IntoPyDict, PyDict};
use pyo3::wrap_pymodule;

fn main() -> Result<(), ()> {
    let gil = Python::acquire_gil();
    let py = gil.python();
    main_(py).map_err(|e| {
        // We can't display Python exceptions via std::fmt::Display,
        // so print the error here manually.
        e.print_and_set_sys_last_vars(py);
    })
}

fn main_(py: Python) -> PyResult<()> {
    let matches = App::new("charman")
        .version("0.1.0")
        .author("Taylor C. Richberger <taywee@gmx.com>")
        .about("The D&D Character Manager")
        .arg(
            Arg::with_name("CHARACTER")
                .help("character directory input")
                .required(false)
                .index(1),
        )
        .get_matches();

    init_charman(py)?;
    let globals = [("code", py.import("code")?)].into_py_dict(py);
    py.run("code.interact()", Some(globals), None)?;
    Ok(())
}
