mod ability;
mod alignment;
mod items;

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::u32;

use crate::weight::Weight;

pub use ability::Abilities;
pub use ability::Ability;
pub use ability::Skill;
pub use alignment::Alignment;
pub use items::Item;
pub use items::Items;

/// The character sheet class.
///
/// This manages all the internal state for a single character, including inventory, stats, and
/// various other aspects.  For many of these, things are managed as Rc and RefCell objects, so
/// that potential mutable references to them may be taken (this is useful, for instance, when you
/// want to keep multiple bags open at a time and possibly modify their contents).
#[derive(Default, Debug, Clone, Deserialize, Serialize)]
pub struct Character {
    pub name: String,
    pub race: String,
    pub experience: u32,
    pub inspiration: u16,
    pub armor_class: u8,
    pub speed: u16,

    /// Base for hitpoint maximum, unmodified by constitution score or bonuses
    pub base_hit_point_maximum: u16,

    /// Can be negative
    pub hit_point_bonus: i16,
    pub temporary_hit_points: u16,

    /// Current hit points.  Expected to be lower than the maximum
    pub hit_points: u16,

    /// Hit dice left.  The total is just the player's level
    pub hit_dice: u8,

    /// Number of sides on a hit die
    pub hit_die_size: u8,

    pub death_save_successes: u8,
    pub death_save_failures: u8,

    pub alignment: Alignment,
    pub body_weight: f32,
    pub abilities: Abilities,
    pub background: String,
    /// Class ID, or class.subclass ID
    pub class: String,
    pub details: Vec<(String, String)>,

    /// Features and traits
    pub features: Vec<(String, String)>,

    // Instead of a hashset, since proficiency can be doubled in some circumstances.
    // Language is considered a proficiency.
    pub proficiencies: HashMap<String, u8>,

    // Later may be reduced to a HashSet, but currently traits include their descriptions
    pub traits: Vec<(String, String)>,
    pub items: Items,
}

impl Character {
    pub fn level(&self) -> u8 {
        match self.experience {
            u32::MIN..=299 => 1,
            300..=899 => 2,
            900..=2699 => 3,
            2700..=6499 => 4,
            6500..=13999 => 5,
            14000..=22999 => 6,
            23000..=33999 => 7,
            34000..=47999 => 8,
            48000..=63999 => 9,
            64000..=84999 => 10,
            85000..=99999 => 11,
            100000..=119999 => 12,
            120000..=139999 => 13,
            140000..=164999 => 14,
            165000..=194999 => 15,
            195000..=224999 => 16,
            225000..=264999 => 17,
            265000..=304999 => 18,
            305000..=354999 => 19,
            355000..=u32::MAX => 20,
        }
    }

    pub fn proficiency(&self) -> u8 {
        (self.level() - 1) / 4 + 2
    }

    pub fn set_proficiency<S>(&mut self, label: S, level: u8) -> Option<u8>
    where
        S: Into<String>,
    {
        self.proficiencies.insert(label.into(), level)
    }

    /// Get the proficiency level for a given proficiency label
    pub fn proficiency_level<S: AsRef<str>>(&self, proficiency: S) -> u8 {
        self.proficiencies
            .get(proficiency.as_ref())
            .cloned()
            .unwrap_or(0)
    }

    /// Get the proficiency bonus for a given proficiency label
    pub fn proficiency_bonus<S: AsRef<str>>(&self, proficiency: S) -> u8 {
        self.proficiency_level(proficiency) * self.proficiency()
    }

    /**
     * Find an ability modifier
     */
    pub fn ability_modifier<S>(&self, id: S) -> Option<i8>
    where
        S: AsRef<str>,
    {
        let id_ref = id.as_ref();
        for ability in self.abilities.iter() {
            if ability.id() == id_ref {
                return Some(ability.modifier());
            }
        }
        None
    }

    /**
     * Find a skill modifier without proficiency
     */
    pub fn raw_skill_modifier<S>(&self, id: S) -> Option<i8>
    where
        S: AsRef<str>,
    {
        let id_ref = id.as_ref();
        for ability in self.abilities.iter() {
            for skill in ability.skills() {
                if skill.id() == id_ref {
                    return Some(ability.modifier());
                }
            }
        }
        None
    }

    /**
     * Get the total modifier for the skill, including proficiency.
     */
    pub fn skill_modifier<S>(&self, id: S) -> i8
    where
        S: AsRef<str>,
    {
        let id_ref = id.as_ref();
        let ability_modifier = self.raw_skill_modifier(id_ref).unwrap_or(0);
        let skill_bonus = self.proficiency_bonus(format!("skill.{}", id_ref));
        ability_modifier + skill_bonus as i8
    }

    /**
     * Get the hit point maximum, including constitution and bonus modification
     */
    pub fn hit_point_maximum(&self) -> i16 {
        let con_modifier = self.ability_modifier("constitution").unwrap_or(0);
        let con_modifier = con_modifier as i16 * self.level() as i16;
        self.base_hit_point_maximum as i16 + self.hit_point_bonus + con_modifier
    }
}

impl Weight for Character {
    fn weight(&self) -> f32 {
        self.body_weight + self.items.weight()
    }
}
